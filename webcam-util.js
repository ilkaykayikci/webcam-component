



export function run(video){
    
    
    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true })
          .then(function (stream) {
            video.srcObject = stream;
            video.play();
          })
          .catch(function (err0r) {
            console.log("Something went wrong!");
          });
      }
         
       
    
}

export function stop(video) {
  const stream = video.srcObject;

  if (stream != null) {
    const tracks = stream.getTracks();

    tracks.forEach(function(track) {
      track.stop();
    });
  
    video.srcObject = null;
}

}

