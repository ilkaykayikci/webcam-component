/**
 * @license
 * Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */

import {LitElement, html, css} from 'lit-element';
import {run, stop} from './webcam-util';

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class WebcamElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
    `;
  }

  static get properties() {
    return {
     
    };
  }

  constructor() {
    super();
     }

  render() {
    return html`
      <div class="videowrap">
        <video playsinline autoplay id="videoElement"> 
        </video>
      </div>
      
      <div class="controller">
        <button @click=${this._onClickStart} part="button">Webcam starten!</button>
        <button @click=${this._onClickStop} part="button">Webcam stoppen!</button>
        <button @click=${this._onClickSnap} part="button">Bild aufnehmen!</button>
      </div>

      <canvas id="canvas" width="640" height="480"></canvas>
      
      `;
  }

  
  _onClickStart() {
    var video = this.shadowRoot.getElementById('videoElement');
    run(video);
  }

  _onClickStop() {
    var video = this.shadowRoot.getElementById('videoElement');
    stop(video);
  }

  _onClickSnap() {
    var video = this.shadowRoot.getElementById('videoElement');
    var canvas = this.shadowRoot.getElementById('canvas');
    var context = canvas.getContext('2d');
    context.drawImage(video, 0, 0, 640 , 480 );  
  }

}

customElements.define('webcam-element', WebcamElement);
