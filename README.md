# webcam-component

This component displays video from the webcam and it allows to take still images.

# How to use

Use either yarn or npm to install.

yarn add https://gitlab.com/ilkaykayikci/webcam-component.git

or

npm install https://gitlab.com/ilkaykayikci/webcam-component.git --save
